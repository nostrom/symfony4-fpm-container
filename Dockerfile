FROM php:7.3.0-fpm-alpine3.8

WORKDIR '/app'

RUN apk update && apk upgrade && \
    apk add --no-cache \
    curl \
    php7-curl \
    bash \
    yaml-dev \
    bzip2 \
    git \
    make \
    unzip \
    wget

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
